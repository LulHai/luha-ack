variable "vpc_id" {
  default = ""
}

variable "vpc_cidr" {
  default = "192.168.0.0/16"
}

variable "vswitch_ids" {
  default = []
}

variable "vswitch_cidrs" {
  default = ["192.168.1.0/24", "192.168.2.0/24"]
}

variable "zone_id" {
  default = ["eu-central-1a", "eu-central-1b"]
}

# If there is not specifying vpc_id, the module will launch a new vpc
resource "alicloud_vpc" "vpc" {
  vpc_name   = "labex_vpc"
  count      = var.vpc_id == "" ? 1 : 0
  cidr_block = var.vpc_cidr
}

# According to the vswitch cidr blocks to launch several vswitches
resource "alicloud_vswitch" "vswitches" {
  count             = length(var.vswitch_ids) > 0 ? 0 : length(var.vswitch_cidrs)
  vpc_id            = var.vpc_id == "" ? join("", alicloud_vpc.vpc.*.id) : var.vpc_id
  cidr_block        = element(var.vswitch_cidrs, count.index)
  zone_id           = element(var.zone_id, count.index)
}
