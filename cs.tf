variable "worker_instance_types" {
  default = ["ecs.hfc6.large", "ecs.c6.large"]
}

variable "worker_number" {
  default = "3"
}

variable "node_cidr_mask" {
  default = "24"
}

variable "enable_ssh" {
  default = "false"
}

variable "install_cloud_monitor" {
  default = "true"
}

variable "cpu_policy" {
  default = "none"
}

variable "proxy_mode" {
  default = "ipvs"
}

variable "password" {
  default = "Aliyun-test"
}

variable "service_cidr" {
  default = "172.16.0.0/16"
}

variable "pod_cidr" {
  default = "10.67.0.0/16"
}

variable "cluster_addons" {
  description = "Addon components in kubernetes cluster"

  type = list(object({
    name      = string
    config    = string
  }))

  default = [
    {
      "name"     = "flannel",
      "config"   = "",
    }
  ]
}

resource "alicloud_cs_managed_kubernetes" "k8s" {
  name_prefix           = "LuHa-"
  # version can not be defined in variables.tf. Options: 1.22.3-aliyun.1|1.20.11-aliyun.1
  version               = "1.22.3-aliyun.1"
  worker_vswitch_ids    = length(var.vswitch_ids) > 0 ? split(",", join(",", var.vswitch_ids)): length(var.vswitch_cidrs) < 1 ? [] : split(",", join(",", alicloud_vswitch.vswitches.*.id))
  worker_instance_types = var.worker_instance_types
  worker_number         = var.worker_number
  node_cidr_mask        = var.node_cidr_mask
  enable_ssh            = var.enable_ssh
  install_cloud_monitor = var.install_cloud_monitor
  cpu_policy            = var.cpu_policy
  proxy_mode            = var.proxy_mode
  password              = var.password
  service_cidr          = var.service_cidr
  pod_cidr              = var.pod_cidr

  dynamic "addons" {
      for_each = var.cluster_addons
      content {
        name                    = lookup(addons.value, "name", var.cluster_addons)
        config                  = lookup(addons.value, "config", var.cluster_addons)
      }
  }
  runtime = {
    name    = "docker"
    version = "19.03.5"
  }
}

output "cluster_id" {
  value = alicloud_cs_managed_kubernetes.k8s.id
}
